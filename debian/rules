#!/usr/bin/make -f
#DH_VERBOSE = 1

# Enable hardening build flags
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk
include /usr/share/dpkg/pkg-info.mk

UPSTREAM_VERSION = $(shell echo $(DEB_VERSION_UPSTREAM) | sed -e 's/\+.*//')

archconfflags :=

ifeq ($(DEB_HOST_ARCH_OS),linux)
  archconfflags += --enable-alsa
else
  archconfflags += --disable-alsa
endif


ifeq ($(DEB_HOST_ARCH_OS),kfreebsd)
  archconfflags += --enable-oss
else
  archconfflags += --disable-oss
endif

archconfflags += --disable-public-libgsequencer --enable-introspection --enable-libinstpatch --with-poppler --with-gtk-unix-print --enable-upstream-gtk-doc --enable-gtk-doc --enable-gtk-doc-html

# provide some system dependent configuration as a C macro
CPPFLAGS += -DAGS_CSS_FILENAME=\"/usr/share/gsequencer/styles/ags.css\"
CPPFLAGS += -DAGS_ANIMATION_FILENAME=\"/usr/share/gsequencer/images/gsequencer-800x450.png\"
CPPFLAGS += -DAGS_LOGO_FILENAME=\"/usr/share/gsequencer/images/ags.png\"
CPPFLAGS += -DAGS_LICENSE_FILENAME=\"/usr/share/common-licenses/GPL-3\"
CPPFLAGS += -DAGS_ONLINE_HELP_A4_PDF_FILENAME=\"/usr/share/doc/gsequencer-doc/pdf/user-manual-a4.pdf\"
CPPFLAGS += -DAGS_ONLINE_HELP_LETTER_PDF_FILENAME=\"/usr/share/doc/gsequencer-doc/pdf/user-manual-letter.pdf\"

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- $(archconfflags)

override_dh_auto_build:
	dh_auto_build
	$(MAKE) DESTDIR=$$(pwd)/debian/tmp html
	$(MAKE) DESTDIR=$$(pwd)/debian/tmp fix-local-html
	$(MAKE) DESTDIR=$$(pwd)/debian/tmp pdf

override_dh_auto_test:
	xvfb-run --server-args="-screen 0 1920x1080x24" -a dh_auto_test -- CFLAGS="-fpermissive -Wno-error=incompatible-pointer-types -Wno-error=pedantic $(dpkg-buildflags --get CFLAGS)"

override_dh_auto_install:
	dh_auto_install --no-parallel
	$(MAKE) DESTDIR=$$(pwd)/debian/tmp install-html
	$(MAKE) DESTDIR=$$(pwd)/debian/tmp install-pdf
	find $$(pwd)/debian/tmp/usr/share -type f \
		\( -name "htmlhelp.hhp" -or -name "toc.hhc" \) \
		-delete
	rm -f $$(pwd)/debian/tmp/usr/share/doc/libags-doc/api/libags
	rm -f $$(pwd)/debian/tmp/usr/share/doc/libags-audio-doc/api/libags-audio
	rm -f $$(pwd)/debian/tmp/usr/share/doc/libags-gui-doc/api/libags-gui
	find $$(pwd)/debian/tmp/usr/lib -type f -name "*.la" -delete

override_dh_compress:
	dh_compress -Xuser-manual-a4.pdf -Xuser-manual-letter.pdf -A

override_dh_makeshlibs:
	dh_makeshlibs -Xlibgsequencer -- -v$(UPSTREAM_VERSION)

override_dh_clean:
	find docs/ -type f -name "*.stamp" -delete
	find docs/ -type f \( \
		   -name "*-decl-list.txt" \
		-o -name "*-decl.txt" \
		-o -name "*-undeclared.txt" \
		-o -name "*-undocumented.txt" \
		-o -name "*-unused.txt" \
		\) -delete
	find docs/ -type f \( \
		   -name "*.args" \
		-o -name "*.hierarchy" \
		-o -name "*.prerequisites" \
		-o -name "*.signals" \
		\) -delete
	find docs/ -type d -name ".libs" -exec rm -rf {} +
	find docs/ -type d -name "tmpl" -exec rm -rf {} +
	dh_clean

# for integration tests (see tests/ags-integration-unit-test)
pre-integration-test:
	dh_autoreconf
	dh_auto_configure -- --libdir=/usr/lib/$(DEB_HOST_MULTIARCH) --enable-run-functional-tests --enable-run-system-tests $(archconfflags)
